import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

 import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));


// import { LikeComponent } from './app/like.Component';

// let component = new LikeComponent(10,true);
// component.onClick();
// console.log(`likes count: ${component.likeCount}, isSelected: ${component.isSelected}`);
