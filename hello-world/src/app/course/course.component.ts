import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';

@Component({
  selector: 'app-course',
//   template:`
//   <h2>{{title}}</h2>
// <ul>
//     <li *ngFor="let course of courses">
//         {{course
        
//         }}
//     </li>
// </ul>
// `
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  title = "List of Course";
  text = "This is my first application to learn angular basic and andvanced course.";

  courses;

  constructor(service : CourseService) { 
    this.courses = service.getCourses();
  }

  ngOnInit(): void {
  }

}
